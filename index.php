<?php
/* $Id: index.php v. 0.0.0 13/05/2007 mdb Exp $
 * $Author: mdb $
 *
 * insaneQuotes.co.nr Index
 *
 * Written by Filippo Baruffaldi (filippo@baruffaldi.info)
 *
 * PHP released under Creative Commons Attribution-Noncommercial-Share
 * Alike 2.5 Italy License :: http://creativecommons.org/licenses/by-nc-sa/2.5/it/
*/
// Creazione sistema output via xml+xsl per supporto ajax

// Include all libraries and system files
include 'includes.php';

$site = new insaneQuotes();

class insaneQuotes
{
		var $site_renderingtime;
		
		function insaneQuotes() {
				global $site_start, $chiaviget, $SQLStream;
				
				// Check if is a logout session
				if ($chiaviget[0] == "logout") IQ_CORE::logout();
				
				// Start session for cookies and log/login system
				$login = IQ_CORE::login();
				
				// Log the client request
				IQ_CORE::logrequest();
				
				// Print the XHTML header code
				if (!isset($_POST[quote])) print IQ_SITEMASK::htmlinit("");

				// Print the XHTML body code
				print IQ_NAVIGATION::section($chiaviget[0]);

				// Start rendering page timer
				$this->site_renderingtime = IQ_CORE::getmicrotime() - $time_start;
				$this->site_renderingtime = substr($this->site_renderingtime, 0, 5);

				// Print the XHTML footer code
				print IQ_SITEMASK::htmlfooter($time_end);

				// Close the SQL connection
				IQ_SQL::closesql($SQLStream, NULL);
		}
}
?>