<?php
/* $Id: includes.php v. 0.0.1 01:41 15/05/2007 mdb Exp $
 * $Author: mdb $
 *
 * www.insaneQuotes.co.nr Include Scripts File
 *
 * Written by Filippo Baruffaldi (filippo@baruffaldi.info)
 *
 * PHP released under Creative Commons Attribution-Noncommercial-Share
 * Alike 2.5 Italy License :: http://creativecommons.org/licenses/by-nc-sa/2.5/it/
*/

include 'defines.php';
include 'languages/lang.'.SITE_LANG.'.php';
include 'conf.php';
		
// Include all libraries and system files
includedir("libraries");
includedir("system");

// Timing rendering page
$time_start = IQ_CORE::getmicrotime();

// Start SQL connection
$SQLStream = IQ_SQL::create();

// Check if there is to take uploaded file
if (is_uploaded_file($HTTP_POST_FILES['file']['tmp_name'])) {/*
	$path = "uploads/";
	$max_size = 20000000;
	$filename = $HTTP_POST_FILES['file']['name'];
	$filez = $HTTP_POST_FILES['file']['tmp_name'];
	$mh = explode(".", $filename);
	if ($HTTP_POST_FILES['file']['size']>$max_size) $resultfile = "Il file � troppo grosso";
	$resss = copy($HTTP_POST_FILES['file']['tmp_name'], $filez);
	$filetype = strtolower(array_pop($mh));
	$linea = @mysql_fetch_array(IQ_SQL::select(TBMIME, "*", "`ext` LIKE '%".strtolower($filetype)."%'", "1", "id"), MYSQL_ASSOC);
	$mimetype = $linea[mimetype];
	if (empty($mimetype)) $mimetype = "unknown/$filetype";
	if (!$resss) $resultfile = "upload fallito.";*/
}

/* Algoritmo per includere tutti files di una cartella */
function includedir($directory) {
	$dh=opendir($directory);
	while ($file=readdir($dh)) {
			$check = explode(".", $file);
			if ($file != "index.php" && !is_dir("$directory/$file") && array_pop($check) == "php") include "$directory/$file";
	}
	closedir($dh);
}
?>