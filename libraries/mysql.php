<?php
/* $Id: mysql.php v. 0.3.7 30/11/2006 14:03:07 mdb Exp $
 * $Author: mdb $
 *
 * www.Baruffaldi.Info MYSQL Functions library
 *
 * Written by Filippo Baruffaldi (filippo@baruffaldi.info)
 *
 * PHP released under Creative Commons Attribution-Noncommercial-Share
 * Alike 2.5 Italy License :: http://creativecommons.org/licenses/by-nc-sa/2.5/it/
*/


/* SQL Class */
class IQ_SQL 
{
		function checksqlerror($error)
		{
				$check = explode(".", $error);
				//if ($check[0] == "xxx") return "<p style=\"color: red;\"><b>Errore SQL</b>: ".$check[1]."</p>";
		}
	   
	   
		function create()
		{
			/* Tables information */
			define('TBARTICLES', 'bf_articles');
			define('TBCV', 'bf_curriculum');
			define('TBVISITS', 'bf_visits');
			define('TBMIME', 'bf_mime');
			define('TBCATS', 'bf_cats');
			define('TBFILES', 'bf_files');
			$con = @mysql_connect(DBHOST, DBUSER, DBPASS);
			if (!($con === false)) {
			    if (@mysql_select_db(DBNAME, $con) === false) {
				   $ret = 'Could not select database: ' . mysql_error();
				   return $ret;
			    }
			} else {
				exit('xxx.Could not connect on database: ' . mysql_error());
				return 'xxx.Could not connect on database: ' . mysql_error();
			}

		    return $con;
		}

		
		
		function closesql($str, $result)
		{
	       if (!empty($result)) @mysql_free_result($result);
	       @mysql_close($str);
		}

		
		
		function select($table, $col, $where, $limit, $order)
		{
	       $table = explode('?', $table);
			if ($table[0] == "im_forum_users" && $order == "id") $order = "user_id";
		   if ($table[0] == "im_visits") {
			       if (!empty($limit)) {
			             $query = "SELECT $col FROM $table[0] ORDER BY $order DESC LIMIT $limit";
			             if (!empty($where)) $query = "SELECT $col FROM $table[0] WHERE $where ORDER BY $order DESC LIMIT $limit";
			       } else {
			             $query = "SELECT $col FROM $table[0] ORDER BY $order DESC";
			             if (!empty($where)) $query = "SELECT $col FROM $table[0] WHERE $where ORDER BY $order DESC";
			       }
			} elseif ($table[0] == "im_forum_topics") {
			       if (!empty($limit)) {
			             $query = "SELECT $col FROM $table[0] ORDER BY $order DESC LIMIT $limit";
			             if (!empty($where)) $query = "SELECT $col FROM $table[0] WHERE $where ORDER BY $order DESC LIMIT $limit";
			       } else {
			             $query = "SELECT $col FROM $table[0] ORDER BY $order DESC";
			             if (!empty($where)) $query = "SELECT $col FROM $table[0] WHERE $where ORDER BY $order DESC";
			       }
			} elseif ($table[0] == "bf_curriculum") {
			       if (!empty($limit)) {
			             $query = "SELECT $col FROM $table[0] ORDER BY $order DESC LIMIT $limit";
			             if (!empty($where)) $query = "SELECT $col FROM $table[0] WHERE $where ORDER BY $order ASC LIMIT $limit";
			       } else {
			             $query = "SELECT $col FROM $table[0] ORDER BY $order DESC";
			             if (!empty($where)) $query = "SELECT $col FROM $table[0] WHERE $where ORDER BY $order ASC";
			       }
			} else {
			       if (!empty($limit)) {
			             $query = "SELECT $col FROM $table[0] ORDER BY $order DESC LIMIT $limit";
			             if (!empty($where)) $query = "SELECT $col FROM $table[0] WHERE $where ORDER BY $order DESC LIMIT $limit";
			       } else {
			             $query = "SELECT $col FROM $table[0] ORDER BY $order DESC";
			             if (!empty($where)) $query = "SELECT $col FROM $table[0] WHERE $where ORDER BY $order DESC";
			       }
			}
	       if ($table[1] == 'debug') print $query;
	       $result = mysql_query($query);
	        if (empty($result)) return "xxx.[select] details: " . mysql_error();
			   

	       return $result;
		}

		
		
		function insertrow($table, $col, $values)
		{
	       $table = explode('?', $table);
	       $query = "INSERT INTO $table[0] ($col) VALUES ($values)";

	       $result = mysql_query($query)
	           or print('Error: ' . mysql_error() . "<br />Segnala questo errore al webmaster, guadagnerai 10 punti.");
			   
	       if ($table[1] == 'debug') print $query;
	       return $result."###ERROR:".mysql_error();
		}

		
		
		function deleterow($table, $where)
		{
	       $table = explode('?', $table);
	       $query = "DELETE FROM $table[0] WHERE $where";

	       $result = mysql_query($query)
	           or print('Sorry! Can`t work that query[delete] on database<br />Error: ' . mysql_error() . "<br />");
			   
	       if ($table[1] == 'debug') print $query;
	       return $result;
		}

		
		
		function modifyrow($table, $col, $value, $where)
		{
			$table = explode('?', $table);
			$query = "UPDATE $table[0] SET $col = ".'"'.$value.'"'." WHERE $where";

			$result = mysql_query($query)
				or print('Sorry! Can`t work that query[update/set] on database<br />Error: ' . mysql_error() . "<br />");
			   
	       if ($table[1] == 'debug') print $query;
			return $result;
		}

		
		
		function sql_query($query)
		{
			$querya = explode('?', $query);
			$result = mysql_query($querya[0])
				or print('Sorry! Can`t work that query[insane] on database<br />Error: ' . mysql_error() . "<br />");
			   
	        if ($table[1] == 'debug') print $query;
		   
			return $result;
		}
}
?>
