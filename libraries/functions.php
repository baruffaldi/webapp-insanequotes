<?php
/* $Id: functions.php v. 0.3.7 30/11/2006 14:03:07 mdb Exp $
 * $Author: mdb $
 *
 * www.Baruffaldi.Info Functions library
 *
 * Developed by Filippo Baruffaldi (mdb@kimera-lab.com)
 *
 * Code released under the GNU General Public License (GPL)  - http://www.opensource.org/licenses/gpl-license.html
 *
 * You may not reproduce it elsewhere without the prior written permission of the author.
 * However, feel free to study the code and use techniques you learn from it elsewhere.
*/

function sql2date($datetime) 
{
	if (!empty($datetime)) {
		$ehm = explode(" ", $datetime);
		$uh = explode("-", $ehm[0]);
		$ret = "{$uh[2]}/{$uh[1]}/{$uh[0]} {$ehm[1]}";
	}
	return $ret;
}

function cleanquote($quote)
{
	if (!empty($quote)) {
		return str_replace("\n", "<br />", str_replace(" ", "&nbsp;", $quote));
	}
}

function phpbb2login() {
				function encode_ip($dotquad_ip)
				{
					$ip_sep = explode('.', $dotquad_ip);
					return sprintf('%02x%02x%02x%02x', $ip_sep[0], $ip_sep[1], $ip_sep[2], $ip_sep[3]);
				}
				
				$cookiename = "phpbb2mysql";
				$current_time = time();
				$cookiepath = "/";
				$cookiedomain = "";
				$cookiesecure = 0;
				$sessiondata = isset( $HTTP_COOKIE_VARS[$cookiename . '_data'] ) ? unserialize(stripslashes($HTTP_COOKIE_VARS[$cookiename . '_data'])) : array();
				$session_id = isset( $HTTP_COOKIE_VARS[$cookiename . '_sid'] ) ? $HTTP_COOKIE_VARS[$cookiename . '_sid'] : session_id();
				$userdata = @mysql_fetch_array(IQ_SQL::select("im_forum_sessions", "*", "`session_id` = '".session_id()."'", "", "session_id"), MYSQL_ASSOC);
				//$userdata = @mysql_fetch_array(IM_SQL::select(TBUSERS, "*", "`user_id` = ' = '".ID."'", "", "user_id"), MYSQL_ASSOC);
				if (!isset($userdata[session_ip])) {
				IQ_SQL::insertrow("im_forum_sessions", 
																  "`session_id`,`session_user_id`,`session_start`,`session_time`,`session_ip`,`session_page`,`session_logged_in`,`session_admin`", 
																  '"'.session_id().'","'.ID.'","'.time().'","'.time().'","'.encode_ip(IP).'","0","0","0"');
				}
				
				IQ_SQL::modifyrow("im_forum_sessions", "session_logged_in", "1", "session_id = '". session_id() ."'");
				
				if ( $current_time - $userdata['session_time'] > 60 )
				{
					IQ_SQL::modifyrow("im_forum_sessions", "session_time", $current_time, "session_id = '". $session_id ."'");
					IQ_SQL::modifyrow("im_forum_sessions", "session_page", "0", "session_id = '". $session_id ."'");
					IQ_SQL::modifyrow(TBUSERS, "user_session_time", $current_time, "user_id = '". ID ."'");
					IQ_SQL::modifyrow(TBUSERS, "user_session_page", "0", "user_id = '". ID ."'");
				}
				IQ_SQL::deleterow("im_forum_sessions", "session_time < " . (time() - (int) 3600));

				setcookie($cookiename . '_data', serialize($sessiondata), $current_time + 31536000, $cookiepath, $cookiedomain, $cookiesecure);
				setcookie($cookiename . '_sid', $session_id, 0, $cookiepath, $cookiedomain, $cookiesecure);
}
?>