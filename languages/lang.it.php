<?php
/* $Id: lang.it.php, v0.1.7 01:32 15/04/2007 mdb Exp $
 * $Author: mdb $
 *
 * www.insaneQuotes.co.nr Italian Language file
 *
 * Written by Filippo Baruffaldi (filippo@baruffaldi.info)
 *
 * PHP released under Creative Commons Attribution-Noncommercial-Share
 * Alike 2.5 Italy License :: http://creativecommons.org/licenses/by-nc-sa/2.5/it/
*/
define('TXT_SITE_VERSION', "0.0.0");
define('TXT_SITE_LASTUPDATE', "15/05/2007");
define('TXT_SITE_KEYWORDS', "InsaneMinds, InsaneQuotes, Quotes, Citazioni");
define('TXT_SITE_NAME', "InsaneQuotes");
define('TXT_SITE_PAGENAME', "");
define('TXT_SITE_ME', "Filippo Baruffaldi");
define('TXT_META_TITLE', "InsaneQuotes :: Le migliori citazioni della rete!");
define('TXT_META_DESC1', "Raccolta di tutte le migliori citazioni di persone conosciute e non del popolo di internet.");
define('TXT_META_DESC2', "insaneQuotes(InsaneMinds Network) � 2004-2007");
define('TXT_FOOTER_COPY', "<b>insaneQuotes</b>(<a href=\"http://www.insaneminds.org\" title=\"InsaneMinds Network\">InsaneMinds Network</a>) � 2004-2007");
define('SITE_POSTRULES', "<dl id='site_rules'><dd>Inserire un log non piu' lungo di 800 caratteri.</dd><dd>Rimuovete ogni parte non necessaria (risate, discorsi non inerenti alla citazione</dd><dd>Rimuovete eventuali date/ore specificate, a meno che non siano indispensabili per la comprensione della citazione.</dd><dd>Le citazioni di battute unicamente offensive a danni di persone fisiche verranno eliminati a priori</dd><dd>La citazione deve potersi capire anche non conoscendo chi l'ha scritta o il contesto in cui � stata inserita.</dd><dd>Le citazioni inviate che non seguiranno queste regole verranno cancellate.</dd></dl>");
define('TXT_HOME_TXT1', "In questo sito puoi trovare una <b>raccolta</b> costantemente aggiornata <b>di citazioni</b> prese dai piu' comuni strumenti di comunicazione che internet ci offre. Gli \"instant messengers\" come <b>MSN</b>, <b>AIM</b>, <b>GTalk</b> o l'eterna <b>IRC</b> o piu' semplicemente <b>webchats</b>, <b>forums</b> etc etc.<br /><br />Registrandoti potrai inserire e votare anche tu le tue citazioni preferite, le quali verranno prima sottoposte ad un controllo da parte dello staff rispettando specifici criteri di valutazione.<br /><br /><i>Ricordiamo all'utenza che <b>questo sito � parte integrante del network insaneminds.org</b>, pertanto il vostro account funzioner� senza alcun problema su tutti i portali del network </b>InsaneMinds</b>.</i><br /><br />Con l'augurio di un buon intrattenimento ti invitiamo a non aspettare oltre per dare un occhiata alle ultime citazioni arrivate attraverso <a href='?lastquotes' title='InsaneQuotes :: Ultime citazioni inserite'>questo link</a>!<br /><br /><br />InsaneMinds Staff");
define('TXT_NAVBAR_HOME', "Benvenuto su insaneQuotes.co.nr!");
define('TXT_NAVBAR_REGISTER', "Crea un nuovo account InsaneMinds");
define('TXT_NAVBAR_QUOTE', "Citazione");
define('TXT_NAVBAR_QUOTES', "Ricerca citazioni");
define('TXT_NAVBAR_NEWQUOTE', "Inserimento nuova citazione");
define('TXT_NAVBAR_LASTQUOTES', "Ultime citazioni inserite");
define('TXT_NAVBAR_RANDQUOTES', "Citazioni selezionate casualmente");
define('TXT_NAVBAR_RANKQUOTES', "Classifica delle migliori citazioni");
define('TXT_NAVBAR_CONT', "Contattaci!");
define('TXT_NAVBAR_DF', "Pagina non trovata!");
define('TXT_NAVBAR_UC', "Pagina in costruzione");
define('TXT_NAVBAR_UB', "Permesso negato!");
define('TXT_NAVBAR_UA', "Pagina in aggiornamento");
define('TXT_MENU_HOME', "Home");
define('TXT_SITE_DF', "Sono spiacente ma la pagina da Lei richiesta non � disponibile");
define('TXT_SITE_UA', "Questa pagina � in fase di aggiornamento");
define('TXT_SITE_UB', "Non hai l'autorizzazione ad accedere a questa pagina");
define('TXT_SITE_UC', "Questa pagina � in fase di sviluppo");
define('TXT_SITE_EMAIL', "Email");
define('TXT_CRON_BACK', "Torna indietro");