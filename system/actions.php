<?php
/* $Id: actions.php v. 2.8.3 01:43 15/04/2007 mdb Exp $
 * $Author: mdb $
 *
 * www.insaneQuotes.co.nr Actions functions library
 *
 * Written by Filippo Baruffaldi (filippo@baruffaldi.info)
 *
 * PHP released under Creative Commons Attribution-Noncommercial-Share
 * Alike 2.5 Italy License :: http://creativecommons.org/licenses/by-nc-sa/2.5/it/
*/

class IQ_ACTIONS
{
	    function newquote()
	    {
				$idq = md5(rand(1,888888));
				$quote = htmlentities($_POST[quote], ENT_QUOTES);

				IQ_SQL::insertrow(TBQUOTES,
						  "`idq`,`idp`,`date`,`quote`",
						  "\"".$idq."\",\"".ID."\",\"".DATE." ".TIME."\",\"$quote\"");

				return $idq;
		}
}
?>