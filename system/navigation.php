<?php
/* $Id: navigation.php v. 0.6.4 01:43 15/04/2007 mdb Exp $
 * $Author: mdb $
 *
 * www.Baruffaldi.Info Navigation Function library
 *
 * Written by Filippo Baruffaldi (filippo@baruffaldi.info)
 *
 * PHP released under Creative Commons Attribution-Noncommercial-Share
 * Alike 2.5 Italy License :: http://creativecommons.org/licenses/by-nc-sa/2.5/it/
*/

class IQ_NAVIGATION
{
	   function section($section)
	   {
				global $chiaviget;

				switch($section) {
					default:
						print IQ_SITEMASK::htmlopenwindow(TXT_NAVBAR_DF);
						print IQ_SITE::NF();
						print IQ_SITEMASK::htmlclosewindow();
						break;

					case "":
						print IQ_SITEMASK::htmlopenwindow(TXT_NAVBAR_HOME);
						print IQ_SITE::welcome();
						print IQ_SITEMASK::htmlclosewindow();
						break;
						
					case 'feedback':
						print IQ_SITEMASK::htmlopenwindow(TXT_NAVBAR_CONT);
						print IQ_SITE::feedback();
						print IQ_SITEMASK::htmlclosewindow();
						break;

					case 'UC':
						print IQ_SITEMASK::htmlopenwindow(TXT_NAVBAR_UC);
						print IQ_SITE::UC();
						print IQ_SITEMASK::htmlclosewindow();
						break;

					case 'UB':
						print IQ_SITEMASK::htmlopenwindow(TXT_NAVBAR_UB);
						print IQ_SITE::UB();
						print IQ_SITEMASK::htmlclosewindow();
						break;

					case 'UA':
						print IQ_SITEMASK::htmlopenwindow(TXT_NAVBAR_UA);
						print IQ_SITE::UA();
						print IQ_SITEMASK::htmlclosewindow();
						break;
						
					case 'privacyandtos':
						print IQ_SITEMASK::htmlopenwindow(TXT_NAVBAR_REGISTER);
						print IQ_SITE::privacyandtos();
						print IQ_SITEMASK::htmlclosewindow();
						break;
						
					case 'register':
						print IQ_SITEMASK::htmlopenwindow(TXT_NAVBAR_REGISTER);
						print IQ_SITE::register();
						print IQ_SITEMASK::htmlclosewindow();
						break;
						
					case 'activation':
						print IQ_SITEMASK::htmlopenwindow(TXT_NAVBAR_REGISTER);
						print IQ_SITE::activation();
						print IQ_SITEMASK::htmlclosewindow();
						break;
						
					case 'search':
						print IQ_SITEMASK::htmlopenwindow(TXT_NAVBAR_QUOTES);
						print IQ_SITE::searchquotes();
						print IQ_SITEMASK::htmlclosewindow();
						break;
						
					case 'quote':
						print IQ_SITEMASK::htmlopenwindow(TXT_NAVBAR_QUOTE);
						print IQ_SITE::pickquote();
						print IQ_SITEMASK::htmlclosewindow();
						break;
						
					case 'lastquotes':
						print IQ_SITEMASK::htmlopenwindow(TXT_NAVBAR_LASTQUOTES);
						print IQ_SITE::lastquotes();
						print IQ_SITEMASK::htmlclosewindow();
						break;
						
					case 'randquotes':
						print IQ_SITEMASK::htmlopenwindow(TXT_NAVBAR_RANDQUOTES);
						print IQ_SITE::randquotes();
						print IQ_SITEMASK::htmlclosewindow();
						break;
						
					case 'rankquotes':
						print IQ_SITEMASK::htmlopenwindow(TXT_NAVBAR_RANKQUOTES);
						print IQ_SITE::rankquotes();
						print IQ_SITEMASK::htmlclosewindow();
						break;

					case 'newquote':
						if (!isset($_POST[quote])) print IQ_SITEMASK::htmlopenwindow(TXT_NAVBAR_NEWQUOTE);
						print IQ_SITE::newquote();
						if (!isset($_POST[quote])) print IQ_SITEMASK::htmlclosewindow();
						break;
/*
					case 'azione':
						if (NICKNAME == "guest") {
								switch($_GET[azione]) {
									default:
										print BF_SITEMASK::htmlopenwindow("Errore");
										print "Non � stato possibile riconoscere l'azione da lei richiesta.";
										print BF_SITEMASK::htmlclosewindow();
										print "<p id=\"center\"><a href=\"javascript:history.back()\" title=\"Torna indietro\">Torna indietro</a></p>";
										break;
								}
						} else {
								switch($_GET[azione]) {
									default:
										print BF_SITEMASK::htmlopenwindow("Errore");
										print "Non � stato possibile riconoscere l'azione da Lei richiesta.";
										print BF_SITEMASK::htmlclosewindow();
										print "<p id=\"center\"><a href=\"javascript:history.back()\" title=\"Torna indietro\">Torna indietro</a></p>";
										break;

									case "newcv":
										print BF_SITEMASK::htmlopenwindow("Inserimento nuovo curriculum vitae");
										print BF_ACTIONS::newcv();
										print BF_SITEMASK::htmlclosewindow();
										break;

									case "delcv":
										print BF_SITEMASK::htmlopenwindow("Eliminazione curriculum vitae");
										print BF_ACTIONS::delcv();
										print BF_SITEMASK::htmlclosewindow();
										break;

									case "newcat":
										print BF_SITEMASK::htmlopenwindow("Inserimento nuova categoria");
										print BF_ACTIONS::newcat();
										print BF_SITEMASK::htmlclosewindow();
										break;

									case "delcat":
										print BF_SITEMASK::htmlopenwindow("Eliminazione categoria/e");
										print BF_ACTIONS::delcat();
										print BF_SITEMASK::htmlclosewindow();
										break;

									case "newart":
										print BF_SITEMASK::htmlopenwindow("Inserimento nuovo articolo");
										print BF_ACTIONS::newart();
										print BF_SITEMASK::htmlclosewindow();
										break;

									case "delart":
										print BF_SITEMASK::htmlopenwindow("Eliminazione articolo/i");
										print BF_ACTIONS::delart();
										print BF_SITEMASK::htmlclosewindow();
										break;

									case "newfile":
										print BF_SITEMASK::htmlopenwindow("Inserimento nuovo file");
										print BF_ACTIONS::newfile();
										print BF_SITEMASK::htmlclosewindow();
										break;

									case "delfile":
										print BF_SITEMASK::htmlopenwindow("Eliminazione file/s");
										print BF_ACTIONS::delfile();
										print BF_SITEMASK::htmlclosewindow();
										break;
								}
						}
						break;
				*/
				}
	   }
}
?>