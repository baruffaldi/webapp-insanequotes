<?php
/* $Id: stats.php v. 0.0.4 01:41 15/04/2007 mdb Exp $
 * $Author: mdb $
 *
 * www.Baruffaldi.Info Stats Functions
 *
 * Written by Filippo Baruffaldi (filippo@baruffaldi.info)
 *
 * PHP released under Creative Commons Attribution-Noncommercial-Share
 * Alike 2.5 Italy License :: http://creativecommons.org/licenses/by-nc-sa/2.5/it/
*/

class IQ_STATS 
{
		function visitatoritotali()
		{
				$SQLresult = BF_SQL::select(TBVISITS, "*", "", "", "id");
				$visitatori = array();
				
				while ($linea = @mysql_fetch_array($SQLresult, MYSQL_ASSOC)) {
						if (!in_array($linea[ip], $visitatori)) $visitatori[] = $linea[ip];
				}
				$visitatori = count($visitatori);
				print BF_SQL::checksqlerror($SQLresult);
				return $visitatori;
		}
		
		
		
		function visitetotali()
		{
				$SQLresult = BF_SQL::select(TBVISITS, "*", "", "", "id");
				$visite = array();
				
				while ($linea = @mysql_fetch_array($SQLresult, MYSQL_ASSOC)) {
						if (!in_array($linea[session], $visite)) $visite[] = $linea[session];
				}
				$visite = count($visite);
				print BF_SQL::checksqlerror($SQLresult);
				return $visite;
		}
		
		
		
		function paginetotali()
		{
				return @mysql_num_rows(BF_SQL::select(TBVISITS, "*", "", "", "id"));
		}
		
		
		
		function visitepagina()
		{
				return @mysql_num_rows(BF_SQL::select(TBVISITS, "*", "`get` = '{$_SERVER[REQUEST_URI]}'", "", "id")); //  AND `post` = '$post'
		}
}
?>