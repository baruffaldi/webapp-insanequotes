<?php
/* $Id: site.php v. 4.2.8 01:42 15/04/2007 mdb Exp $
 * $Author: mdb $
 *
 * www.insaneQuotes.co.nr Site General Functions library
 *
 * Written by Filippo Baruffaldi (filippo@baruffaldi.info)
 *
 * PHP released under Creative Commons Attribution-Noncommercial-Share
 * Alike 2.5 Italy License :: http://creativecommons.org/licenses/by-nc-sa/2.5/it/
*/

class IQ_SITE
{


	    function register() 
	    {
				if ($_POST[register]) {
						
						// Controlla che i campi obbligatori siano compilati
						if (empty($_POST[username]) || empty($_POST[password]) || empty($_POST[passwordv]) || empty($_POST[user_email]) || empty($_POST[emailv]) || empty($_POST[verify])) $debug = "<h3>ATTENZIONE</h3>::: Registrazione non effettuata, prego <b>compilare TUTTI i campi</b>. ::<br /><br /><hr class='hr_medium'><br /><h4>Correggi e ritenta!</h4><br />";
						
						// Controlla che l'username e l'email non siano gia' presenti nel database
						if (@mysql_num_rows(IQ_SQL::select(TBUSERS, "`user_id`", "`username` = '{$_POST[username]}'", "", "user_id")) >> 0) $debug = "<h3>ATTENZIONE</h3>::: Registrazione non effettuata, <b>nickname gia' esistente</b>. Controlla di non aver gia' creato un account con quel nome altrimenti prova con un altro nickname. ::<br /><br /><hr class='hr_medium'><br /><h4>Correggi e ritenta!</h4><br />";
						if (@mysql_num_rows(IQ_SQL::select(TBUSERS, "`user_id`", "`user_email` = '{$_POST[user_email]}'", "", "user_id")) >> 0) $debug = "<h3>ATTENZIONE</h3>::: Registrazione non effettuata, <b>indirizzo email gi� utilizzato</b> per un altro profilo. Controlla di non aver gia' creato un account con quel nome altrimenti utilizza un altro indirizzo. ::<br /><br /><hr class='hr_medium'><br /><h4>Correggi e ritenta!</h4><br />";
						
						if (!eregi("^[0-9a-zA-Z_-]{4,18}$", $_POST[username])) $debug = "<h3>ATTENZIONE</h3>::: Il <b>nickname deve essere di almeno 4 caratteri</b> e puo' contenere solamente lettere numeri e questi due caratteri: - _ ::<br /><br /><hr class='hr_medium'><br /><h4>Correggi e ritenta!</h4><br />";
						if (!eregi("^[0-9a-zA-Z_-]{7,18}$", $_POST[password])) $debug = "<h3>ATTENZIONE</h3>::: La <b>password deve essere di almeno 8 caratteri</b> e puo' contenere solamente lettere numeri e questi due caratteri: - _ ::<br /><br /><hr class='hr_medium'><br /><h4>Correggi e ritenta!</h4><br />";
						if (!eregi("^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$", $_POST[user_email])) $debug = "<h3>ATTENZIONE</h3>::: Registrazione non effettuata, al fine di una corretta registrazione <b>inserire un email valida</b>, grazie. ::<br /><br /><hr class='hr_medium'><br /><h4>Correggi e ritenta!</h4><br />";
						if ($_POST[password] != $_POST[passwordv] || $_POST[user_email] != $_POST[emailv]) $debug = "<h3>ATTENZIONE</h3>::: Registrazione non effettuata, <b>password e/o email di verifica non coincidono</b>. ::<br /><br /><hr class='hr_medium'><br /><h4>Correggi e ritenta!</h4><br />";
						
						if (!$debug) {
								// Inserimento nuovo profilo
								$_POST[user_birthday] = sprintf('%02d%02d%04d',$_POST[mese],$_POST[giorno],$_POST[anno]);
								$nascita = "{$_POST[giorno]}/{$_POST[mese]}/{$_POST[anno]}";
								$_POST[username] = ltrim(rtrim(str_replace(" ", "", $_POST[username])));
								$_POST[password] = md5($_POST[password]);
								$attivazione = md5(rand(0, 8888888));
								
								IQ_SQL::insertrow(TBUSERS,
												  "`data`,`user_active`,`username`,`user_password`,`user_email`,`nome`,`cognome`,`sesso`,`nascita`,`localita`,`provincia`,`statocivile`,`user_msnm`,`user_aim`,`user_icq`,`user_yim`,`webcam`,`descrizione`,`user_lang`,`user_allowhtml`,`user_allow_pm`,`user_notify`,`user_notify_pm`,`user_allowbbcode`,`user_allowsmile`,`user_allowavatar`,`user_allow_viewonline`,`user_popup_pm`,`user_rank`,`user_unread_privmsg`,`user_last_privmsg`,`user_birthday`",
												  '"'.DATE." ".TIME.'","'.$attivazione.'","'.$_POST[username].'","'.$_POST[password].'","'.$_POST[user_email].'","'.$_POST[nome].'","'.$_POST[cognome].'","'.$_POST[sesso].'","'.$nascita.'","'.$_POST[localita].'","'.$_POST[provincia].'","'.$_POST[statocivile].'","'.$_POST[user_msnm].'","'.$_POST[user_aim].'","'.$_POST[user_icq].'","'.$_POST[user_yim].'","'.$_POST[webcam].'","'.$_POST[descrizione].'","'."italian".'","'."0".'","'."0".'","0","0","1","1","1","1","0","0","0","0","'.$_POST[user_birthday].'"');
												  
								// Invio email di attivazione profilo
								$id = @mysql_insert_id();
								
								$messaggio = "Buongiorno {$_POST[nome]},\n\nla ringraziamo per essersi iscritto alla nostra 'community', la sua iscrizione � ormai giunta al termine.\n\n".BASELINK."?activation&id=$id&codice=$attivazione\n\nLa preghiamo di cliccare sul link sopracitato per attivare il profilo e per poter usufruire di tutti i servizi offerti da InsaneMinds.oRg tra i quali un blog ed una galleria di immagini personale.\n\nBuona navigazione,\n\n\nInsaneMinds Staff";
								$mail = IQ_CORE::sendmail("insaneminds.org",$_POST[user_email],"InsaneMinds.oRg :: Attivazione profilo: {$_POST[username]}", $messaggio);
								if (!$mail) $mail = NULL;
								if ($mail) $mail = "<h3>ATTENZIONE</h3>: sono stati riscontrati problemi con l'invio dell'email, controllare che l'indirizzo email sia corretto<br /><br />";
								if (!empty($id)) return "$mail :: Grazie per esserti registrato! ::<br /> La registrazione � stata effettuata con successo.<br />Adesso riceverai un email con un link per l'attivazione dell'account alla casella di posta: <i>{$_POST[user_email]}</i><br /><br />In caso non ricevessi l'email entro 24 ore o volessi specificare un'altra email puoi farlo attraverso il link <a href='http://www.insaneminds.org/?recuperoattivazione' title='Recupero Attivazione'>recupera attivazione</a>.<br /><br />[<b>N.B.</b> <i>� possibile che providers come Hotmail ed altri classifichino l'email come 'Posta indesiderata/spam', pertanto controllare anche in quella cartella</i>]<br /><br />Buon intrattenimento con InsaneMinds!";
						}

				}
				
				$ret = "$debug
				Per registrarsi al network InsaneMinds � sufficiente compilare i seguenti campi in maniera corretta.<br />Vi ricordiamo che se avete gia' un account a qualsiasi portale del network InsaneMinds.oRg potete usarlo anche qui evitando la registrazione di un ulteriore profilo<br /><br />
				<form method='post' action='?register'>
				<input name='register' type='hidden' id='input_textbig' value='1' />
				<div id='register'>
						<div id='register_left'>
							<div id='register_row'>Nickname (aZ,123,-_)</div>
							<div id='register_row'>Password (aZ,123,-_)</div>
							<div id='register_row'>Verifica Password</div>
							<div id='register_row'>Email</div>
							<div id='register_row'>Verifica Email</div>
						</div>
						<div id='register_right'>
							<div id='register_row'><input name='username' type='text' id='input_textbig' value='{$_POST[username]}' size='15' maxlength='20' /></div>
							<div id='register_row'><input name='password' type='password' id='input_textbig' value='{$_POST[password]}' size='15' maxlength='20' /></div>
							<div id='register_row'><input name='passwordv' type='password' id='input_textbig' value='{$_POST[passwordv]}' size='15' maxlength='20' /></div>
							<div id='register_row'><input name='user_email' type='text' id='input_textbig' value='{$_POST[user_email]}' size='15' maxlength='40' /></div>
							<div id='register_row'><input name='emailv' type='text' id='input_textbig' value='{$_POST[emailv]}' size='15' maxlength='40' /></div>
						</div>
				</div>
				<div id='register_ptsc'><br />Facendo clic su 'Accetto' di seguito, accetti i Termini di servizio e le Norme sulla privacy riportate in <a href='?privacyandtos' alt='TOS e Norme sulla Privacy'>>questa pagina<</a>.<br /><input name='verify' type='checkbox'> Ai sensi dell'art. 1341 del codice civile, l'utente approva separatamente gli articoli 1 (Descrizione del Servizio), 3 (Utilizzo consentito), 4 (Contenuti del Servizio), 5 (Diritti di propriet�  intellettuale), 9 (Inattivit�  dell'account), 11 (Risoluzione del Contratto), 13 (Legge applicabile; foro competente) delle presenti condizioni.</div>
				<input value='' type='submit' id='input_submit_register'>
				</form>";
				
				return $ret;
		}
		
	    function activation() 
	    {
				// Controlla che ci siano i dati per l'attivazione
				if (empty($_GET[id]) || empty($_GET[codice])) return "<h3>ATTENZIONE</h3>::: Non � stato possibile effettuare l'attivazione a causa della mancanza di alcuni dati sensibili per la buona riuscita dell'operazione. ::";
				$linea = @mysql_fetch_array(IQ_SQL::select(TBUSERS, "`username`,`user_active`", "`user_id` = '{$_GET[id]}'", "1", "user_id"), MYSQL_ASSOC);

				// Controlla che non sia gia' stato attivato
				if ($linea[user_active] == "1") return "<h3>ATTENZIONE</h3>::: Non � stato possibile effettuare l'attivazione in quanto il profilo numero {$_GET[id]} corrispondente al nickname: {$linea[username]}, risulta gi� attivo. ::";
				
				// Controlla che il codice attivazione sia giusto
				if ($linea[user_active] != $_GET[codice]) return "<h3>ATTENZIONE</h3>::: Non � stato possibile effettuare l'attivazione in quanto il codice di attivazione non risulta essere corretto. ::<br />:: Se hai gia' tentato il \"recupero attivazione\" contatta il webmaster ::";
				
				// Attiva profilo
				IQ_SQL::modifyrow(TBUSERS, "`user_active`", "1", "`user_id` = '{$_GET[id]}'");
				
				return ":: Attivazione effettuata con successo. ::<br />Da adesso potrai usufruire di tutti i servizi di InsaneMinds.oRg.<br />Enjoy!! ;)<br /><br /></p><meta http-equiv='Refresh' content='5;url=\"http://www.insaneQuotes.co.nr\"'>";
		}
		
		function searchquotes ()
		{
				$result = IQ_SQL::select(TBQUOTES, "`idq`,`date`,`quote`,`score`", "`quote` LIKE '%{$_POST[search]}%'", NULL, "id");
				
				while($linea = mysql_fetch_array($result, MYSQL_ASSOC)) {
						$quotecut = substr(0,32, $linea[quote]);
						
						$quote = cleanquote($linea[quote]);
						
						$ret .= "
						<div class='quote'>
							<div class='quote_text'>
									<p>$quote</p>
							</div>
							<div class='quote_info'>
									<p>Data: {$linea[date]} :: <a href='?quote={$linea[idq]}' title='$quotecut'>Link</a> :: Punteggio: {$linea[score]} :: Vota: 1 | 2 | 3 | 4 | 5</p>
							</div>
						</div>
						";
				}
				
				return $ret;
		}
		
		function pickquote ()
		{
				$linea = mysql_fetch_array(IQ_SQL::select(TBQUOTES, "`date`,`quote`,`score`", "`idq` = '{$_GET[quote]}'", NULL, "id"), MYSQL_ASSOC);

				$quotecut = substr(0,32, $linea[quote]);
				$quote = cleanquote($linea[quote]);
				
				$ret = "
				<div class='quote'>
					<div class='quote_text'>
							<p>$quote</p>
					</div>
					<div class='quote_info'>
							<p>Data: <i>{$linea[date]}</i> :: <a href='?quote={$_GET[quote]}' title='$quotecut'>Link</a> :: Punteggio: {$linea[score]} :: Vota: 1 | 2 | 3 | 4 | 5</p>
					</div>
				</div>
				";
				
				return $ret;
		}

		function lastquotes ()
		{
				$result = IQ_SQL::select(TBQUOTES, "`idq`,`date`,`quote`,`score`", NULL, NULL, "id");
				
				while($linea = mysql_fetch_array($result, MYSQL_ASSOC)) {
						$quotecut = substr(0,32, $linea[quote]);
						
						$quote = cleanquote($linea[quote]);
						
						$ret .= "
						<div class='quote'>
							<div class='quote_text'>
									<p>$quote</p>
							</div>
							<div class='quote_info'>
									<p>Data: {$linea[date]} :: <a href='?quote={$linea[idq]}' title='$quotecut'>Link</a> :: Punteggio: {$linea[score]} :: Vota: 1 | 2 | 3 | 4 | 5</p>
							</div>
						</div>
						";
				}
				
				return $ret;
		}
		
		function randquotes ()
		{
				$result = IQ_SQL::select(TBQUOTES, "*", NULL, NULL, "id");

				while ($linea = @mysql_fetch_array($result, MYSQL_ASSOC)) {
					if (!empty($linea[quote])) $id[] = $linea[id];
				}
				$max = count($id) -1;
				
				for ($i = 0; $i <= 9; $i++) {
						$idc = rand(0, $max);
						$idz = $id[$idc];
						
						$linea = mysql_fetch_array(IQ_SQL::select(TBQUOTES, "`date`,`quote`,`score`", "`id` = '$idz'", NULL, "id"), MYSQL_ASSOC);

						$quotecut = substr(0,32, $linea[quote]);
						$quote = cleanquote($linea[quote]);

						$ret .= "
						<div class='quote'>
							<div class='quote_text'>
									<p>$quote</p>
							</div>
							<div class='quote_info'>
									<p>Data: <i>{$linea[date]}</i> :: <a href='?quote={$_GET[quote]}' title='$quotecut'>Link</a> :: Punteggio: {$linea[score]} :: Vota: 1 | 2 | 3 | 4 | 5</p>
							</div>
						</div>
						";
				}
				
				return $ret;
		}
		
		function rankquotes ()
		{
				$result = IQ_SQL::select(TBQUOTES, "`idq`,`date`,`quote`,`score`", NULL, "10", "score");
				$n = 1;
				
				while($linea = mysql_fetch_array($result, MYSQL_ASSOC)) {
						$quotecut = substr(0,32, $linea[quote]);
						$quote = cleanquote($linea[quote]);
						
						$ret .= "
						<div class='quote'>
							<div class='quote_title'>
									<p><b>$n</b>� Posto</p>
							</div>
							<div class='quote_text'>
									<p>$quote</p>
							</div>
							<div class='quote_info'>
									<p>Data: <i>{$linea[date]}</i> :: <a href='?quote={$linea[idq]}' title='$quotecut'>Link</a> :: Punteggio: {$linea[score]} :: Vota: 1 | 2 | 3 | 4 | 5</p>
							</div>
						</div>
						";
						
						$n++;
				}
				
				return $ret;
		}
		
		function newquote ()
		{
				if (ID != 0) 
				{
						if (!empty($_POST[quote])) {
								$idq = IQ_ACTIONS::newquote($_POST[quote]);
								$_POST[quote] = NULL;
								header("Location: {$_SERVER[HTTP_REFERER]}&ok=$idq");
						} else {
								if (isset($_GET[ok])) {
										$ret = "
										<h3>Grazie, la tua citazione � stata inserita!</h3><br />
										<hr class='hr_medium'><br />
										<h4>Anteprima citazione</h4>";
										
										$linea = mysql_fetch_array(IQ_SQL::select(TBQUOTES, "`date`,`quote`,`score`", "`idq` = '{$_GET[ok]}'", NULL, "id"), MYSQL_ASSOC);
										
										$quotecut = substr(0,32, $linea[quote]);
										$quote = cleanquote($linea[quote]);
										
										$ret .= "
										<div class='quote'>
											<div class='quote_text'>
													<p>$quote</p>
											</div>
											<div class='quote_info'>
													<p>Data: <i>{$linea[date]}</i> :: <a href='?quote={$linea[idq]}' title='$quotecut'>Link</a> :: Punteggio: {$linea[score]} :: Vota: 1 | 2 | 3 | 4 | 5</p>
											</div>
										</div>
										";
										
										$ret .= "<hr class='hr_medium'><br />";
								}
								$ret .= "
												".SITE_POSTRULES."
										<form method='post' action='?{$_SERVER[QUERY_STRING]}'>
												Incolla nel riquadro sottostante la tua citazione secondo i criteri sopracitati.<br /><br />
												<textarea name='quote' cols='70' rows='10' class='textarea' id='input_textarea'>{$_POST[quote]}</textarea><br />
												<input class='input_submit' value='oK!' type='submit'>
										</form>";
						}

						return $ret;
				} else {
						return "</h4>Autorizzazione negata.</h4><br />Utente non riconosciuto!<br />Per poter inserire delle nuove citazioni devi essere un utente registrato!<br /><br />In caso contrario, rieffettua il login.<br /><br /><br />InsaneMinds Staff";
				}
		}
		
		function welcome ()
		{
				return TXT_HOME_TXT1;
		}

		function feedback ()
		{
				$ret = "
						<h3>".TXT_SITE_EMAIL."</h3>: <a href='mailto:filippo@baruffaldi.info' title='filippo@baruffaldi.info'>filippo@baruffaldi.info</a>";

				return $ret;
		}

		function UC ()
		{
				return TXT_SITE_UC;
		}


		function UB ()
		{
				return TXT_SITE_UB;
		}


		function UA ()
		{
				return TXT_SITE_UA;
		}


		function NF ()
		{
				return TXT_SITE_DF;
		}
		
	    function privacyandtos() 
	    {
				return "<h3>Condizioni e Termini di utilizzo dei servizi</h3><br /><br />
				Ti diamo il benvenuto su <i>Insaneminds.org</i>, il sito che consente agli adulti di incontrarsi online. Il servizio � gestito da <i>Insaneminds.org</i>.<br />
				<br />
				Questa informativa legale fa riferimento all'intero contenuto del sito Web attivo al dominio dominio <i>Insaneminds.org</i> ('sito Web') ed a tutta la corrispondenza tramite posta elettronica che potr� intercorrere fra noi e te (l'Utente). Prima di utilizzare questo sito Web, leggi attentamente tutti i termini e le condizioni. L'utilizzo del sito Web implica l'accettazione dei termini, senza alcuna eccezione o modifica, anche nel caso decidessi di non registrarti come utente, e ti rende automaticamente parte del contratto. Se non accetti questi termini, evita di utilizzare il sito Web.
				<br />
				Completando la procedura di registrazione online, l'affiliazione viene attivata e si considera come accettato un contratto, vincolante ai fini legali, per la fornitura del servizio <i>Insaneminds.org</i> (il 'Servizio').<br />
				<ol id='listnum'>
				<li><b>Ammissibilit�</b>. Per effettuare la registrazione a <i>Insaneminds.org</i> o utilizzare il sito Web � necessario avere compiuto 18 anni. L'affiliazione al Servizio non verr� considerata valida se ottenuta in contravvenzione alle condizioni previste. Utilizzando il sito Web, l'Utente dichiara e garantisce di avere il diritto, l'autorit� e la capacit� di sottoscrivere il Contratto e di accettarne tutti i termini e le condizioni.</li>
				<li><b>Registrazione ed iscrizione / Ambito del servizio</b>. L'affiliazione al servizio � gratuita. Un Utente non iscritto potr� utilizzare alcune delle funzioni disponibili all'interno del Servizio. Per accedere alle funzioni ed ai servizi aggiuntivi, inclusa la possibilit� di comunicare con altri utenti, � necessario pagare l'iscrizione al Servizio. Per informazioni sui piani di iscrizione e sui relativi costi, vedere la pagina delle iscrizioni. L'elenco dei costi di iscrizione � parte integrante di questo Contratto. Ci riserviamo il diritto di modificare le tariffe di iscrizione al Servizio in qualsiasi momento, impegnandoci a fornire agli utenti un ragionevole preavviso. Qualora le eventuali modifiche ai costi di iscrizione risultassero inaccettabili, l'Utente potr� rescindere la propria iscrizione, scrivendo all'indirizzo riportato di sopra, oppure inviando un'email all'indirizzo help@<i>Insaneminds.org</i>.
				<br /><b>Ambito del servizio</b>. Il Servizio consente all'utente di contattare altri utenti. <i>Insaneminds.org</i> non � un'agenzia matrimoniale e non ha alcun obbligo di mediazione fra i vari utenti.
				<br /><b>Richiesta di informazioni aggiornate</b>. PER L'ACCOUNT DI FATTURAZIONE, L'UTENTE � TENUTO A FORNIRE INFORMAZIONI AGGIORNATE, COMPLETE ED ACCURATE. TUTTI I DATI DOVRANNO ESSERE TEMPESTIVAMENTE MODIFICATI PER MANTENERE L'ACCOUNT DI FATTURAZIONE AGGIORNATO, COMPLETO ED ACCURATO (DEVONO ESSERE SEGNALATE EVENTUALI MODIFICHE APPORTATE ALL'INDIRIZZO DI FATTURAZIONE, AL NUMERO O ALLA SCADENZA DELLA CARTA DI CREDITO). INOLTRE, L'UTENTE DOVR� INFORMARE QUANTO PRIMA <i>Insaneminds.org</i> DI EVENTUALI ANNULLAMENTI DEL METODO DI PAGAMENTO (AD ESEMPIO, A CAUSA DI UNA PERDITA O DI UN FURTO) O DI POTENZIALI RISCHI DI VIOLAZIONE DELLA SICUREZZA, COME AD ESEMPIO LA DIVULGAZIONE O L'UTILIZZO NON AUTORIZZATO DEL NOME UTENTE O DELLA PASSWORD. TALI INFORMAZIONI POSSONO ESSERE MODIFICATE NEL SITO WEB. IN CASO DI MANCATA FORNITURA A <i>Insaneminds.org</i> DELLE INFORMAZIONI RICHIESTE, L'UTENTE ACCETTA CHE SUL PROPRIO ACCOUNT DI FATTURAZIONE CONTINUI A ESSERE ADDEBITATO L'EVENTUALE UTILIZZO DEL SERVIZIO, A MENO CHE L'ISCRIZIONE AL SERVIZIO NON SIA STATA REVOCATA (CON EVENTUALE CONFERMA SCRITTA SU RICHIESTA DI <i>Insaneminds.org</i>).</li>
				<li><b>Termini e rescissione</b>. Il presente Contratto sar� valido ed effettivo finch� l'utente sar� iscritto al Servizio. In virt� delle Normative di protezione del consumatore (vendita a distanza) 2000 (come emendate) del Italia, l'utente pu� posporre l'inizio del Servizio fino alla scadenza del periodo di prova di sette giorni lavorativi. Tuttavia, qualora l'utente decida di iscriversi ed acceda al sito Web, pu� utilizzare immediatamente il Servizio, rinunciando in questo modo a qualsiasi diritto di annullamento del Servizio in virt� delle normative specificate sopra.
				L'iscrizione pu� terminare in qualsiasi momento e per qualsiasi motivo, con decorrenza dal momento della ricezione della comunicazione di rescissione scritta o per email da parte di <i>Insaneminds.org</i>. L'utente pu� contattare <i>Insaneminds.org</i> all'indirizzo indicato di seguito oppure inviare un'email all'indirizzo help@<i>Insaneminds.org</i>.
				<i>Insaneminds.org</i> potrebbe terminare immediatamente l'iscrizione e l'accesso al Servizio dell'utente per violazione del presente Contratto. In questo caso, l'utente ricever� un avviso di rescissione all'indirizzo email fornito nella richiesta di iscrizione o all'indirizzo email eventualmente fornito in un secondo momento. Qualora l'iscrizione al Servizio venga terminata per violazione del Contratto, <i>Insaneminds.org</i> risarcir� all'Utente gli eventuali pagamenti preliminari effettuati, da cui verranno detratti i costi e le perdite sostenuti da <i>Insaneminds.org</i>. Il presente Contratto pu� essere rescisso da <i>Insaneminds.org</i> in qualsiasi momento con un ragionevole preavviso scritto.
				Qualora l'Utente termini il proprio abbonamento, questo rester� attivo fino alla fine del periodo per il quale � stato effettuato il pagamento prima della rescissione.
				In caso di violazione di uno o pi� termini del presente Contratto da parte dell'Utente (in particolare delle Clausole 6 e 7), <i>Insaneminds.org</i> potrebbe bloccare l'account dell'Utente, eliminare il contenuto proibito, nonch� impedire la pubblicazione di parte o di tutto il profilo dell'Utente. <i>Insaneminds.org</i> potrebbe inoltre bloccare l'accesso dell'Utente a parte del Servizio o all'intero Servizio, in modo permanente o temporaneo.
				<i>Insaneminds.org</i> potrebbe disattivare gli account degli Utenti che non hanno utilizzato il Servizio per un periodo di almeno sei mesi (calcolati dalla data dell'ultima connessione al Servizio), tranne nel caso di iscritti paganti.</li>
				<li><b>Utilizzo non commerciale da parte degli iscritti</b>. Il sito Web � destinato all'utilizzo personale dei soli singoli Utenti e non pu� essere utilizzato a scopi commerciali. Le societ� e/o le imprese non possono effettuare l'iscrizione, n� utilizzare il Servizio o il sito Web per alcuno scopo. L'utilizzo illegale e/o non autorizzato del sito Web, inclusi la raccolta di nomi utente e/o degli indirizzi email degli utenti con mezzi elettronici o altro, al fine di inviare messaggi email indesiderati e di individuazione non autorizzata o di effettuare collegamenti al sito Web, verranno analizzati e verranno adottate le necessarie misure legali, incluse fra le altre quelle di carattere civile, penale e ingiuntivo.</li>
				<li><b>Diritti proprietari sul contenuto di <i>Insaneminds.org</i></b>. <i>Insaneminds.org</i> possiede e mantiene tutti i diritti proprietari del sito Web e del Servizio. Il sito Web contiene materiale protetto da copyright, marchi di fabbrica ed altra propriet� intellettuale oltre ad informazioni di propriet� di <i>Insaneminds.org</i> e dei relativi concessori di licenza. Ad eccezione delle informazioni di dominio pubblico o per le quali � stata fornita un'autorizzazione scritta, all'utente non � consentito copiare, modificare, pubblicare, trasmettere, distribuire, eseguire, riprodurre, concedere in licenza, creare derivati, trasferire la visualizzazione, vendere o rivendere tale propriet� intellettuale o informazioni proprietarie.</li>
				<li><b>Contenuti pubblicati sul sito e contenuto proibito</b>.
				<ol id='listalpha'><li> Le informazioni fornite dall'Utente a <i>Insaneminds.org</i> e pubblicate sul sito Web devono essere accurate e conformi alla realt�.</li>
				<li> Ad eccezione delle informazioni che potrebbero rendere identificabile l'Utente, e che verranno utilizzate in conformit� all'Informativa sulla privacy di <i>Insaneminds.org</i>, tutto il materiale trasmesso o pubblicato sul sito Web verr� considerato non riservato e non proprietario. <i>Insaneminds.org</i> non avr� alcun obbligo nei confronti di tale materiale. <i>Insaneminds.org</i> e le proprie persone designate avranno la libert� di copiare, divulgare, distribuire, incorporare o utilizzare tale materiale e tutti i dati, le immagini, i suoni, il testo ed altri elementi incorporati nel materiale per qualsiasi scopo, commerciale o non commerciale.</li>
				<li> <i>Insaneminds.org</i> pu� controllare ed eliminare contenuti, messaggi, messaggi di <i>Insaneminds.org</i> Messenger, foto o profili (collettivamente denominati 'Contenuti') che, secondo la ragionevole opinione di <i>Insaneminds.org</i>, violino il presente Contratto o che potrebbero risultare offensivi, illegali o violare i diritti, mettere a repentaglio o minacciare la sicurezza degli Utenti.</li>
				<li> L'Utente � l'unico responsabile dei Contenuti pubblicati, inviati, o visualizzati (di seguito 'pubblicati') nel Servizio o trasmessi ad altri Utenti tramite il Servizio. L'Utente garantisce che i Contenuti pubblicati in qualsiasi momento sono (a) accurati, (b) non in violazione del presente Contratto e (c) non dannosi in alcun modo per qualunque persona.</li>
				<li> Pubblicando i Contenuti in un'area pubblica di <i>Insaneminds.org</i>, l'Utente concede automaticamente e garantisce di disporre del diritto di concedere a <i>Insaneminds.org</i> una licenza a livello mondiale, irrevocabile, perpetua, non esclusiva, completamente corrisposta di utilizzo, copia, esecuzione, visualizzazione e distribuzione di tali informazioni e contenuti. L'Utente concede inoltre il diritto di disporre o incorporare tali informazioni e contenuti in altri materiali, nonch� di concedere ed autorizzare sublicenze per quanto sopra indicato.</li>
				<li> Di seguito viene fornito un elenco del tipo di Contenuto non consentito o proibito nel sito Web. <i>Insaneminds.org</i> si riserva il diritto di aggiornare in qualsiasi momento tale elenco, nonch� di investigare ed adottare le misure legali pi� adeguate a propria discrezione contro chiunque violi tali disposizioni, incluse fra le altre, la rimozione delle comunicazioni offensive dal Servizio e l'annullamento dell'iscrizione di tali contravventori. Inclusi Contenuti che:
				<ol id='listdisc'><li> siano palesemente offensivi nei confronti della comunit� online, quali i Contenuti che promuovono il razzismo, il bigottismo, l'odio o la minaccia fisica di qualsiasi tipo contro qualsiasi gruppo o singolo:</li>
				<li> includano il profilo o fotografie di terzi senza la loro autorizzazione.</li>
				<li> molestino o incitino alla molestia di un'altra persona;</li>
				<li> implichino la trasmissione di 'junk mail', 'catene di S. Antonio' o 'spamming';</li>
				<li> trasmettano informazioni notoriamente false, fuorvianti o che istigano ad attivit� illegali o a condotte legate ad abusi, minacce, oscenit�, azioni diffamatorie o denigranti;</li>
				<li> promuovano copie illegali o non autorizzate del lavoro protetto da copyright di un'altra persona, ad esempio la fornitura di programmi informatici piratati o i relativi collegamenti, la fornitura di informazioni atte a superare i dispositivi di protezione dalle copie o la fornitura di musica piratata o collegamenti a file musicali piratati;</li>
				<li> contengano pagine con accesso limitato o protetto da password o pagine o immagini nascoste (non collegate a un'altra pagina accessibile);</li>
				<li> forniscano materiali di sfruttamento sessuale o violento di minorenni o richiedano informazioni personali a minorenni;</li>
				<li> forniscano istruzioni su come perpetrare attivit� illegali quali costruzione o acquisto di armi illegali, violazione della privacy di una persona o fornitura o creazione di virus informatici;</li>
				<li> richiedano ad altri utenti password o informazioni di identificazione personale a scopi commerciali o illegali</li>
				<li> pubblicizzino, senza previo consenso di <i>Insaneminds.org</i>, attivit� commerciali e/o di vendita quali, concorsi, aste, baratti, pubblicit�, e aziende a schema piramidale.</li></ol>
				<li> L'Utente si impegna a utilizzare il Servizio in conformit� a tutte le leggi, le normative e i codici di condotta applicabili.<li>
				<li> L'Utente non potr� includere nel profilo personale numeri telefonici, indirizzi, cognomi URL o indirizzi email.<li>
				<li> L'Utente si impegna a non utilizzare il Servizio per svolgere attivit� pubblicitarie, per forzare altri utenti all'acquisto/vendita di prodotti o servizi o per convincerli a partecipare a feste o altri eventi sociali o in rete che abbiano fini commerciali. E vietato all'Utente inviare ad altri 'lettere di S. Antonio' o 'junk email'.</li></ol>
				Sebbene <i>Insaneminds.org</i> non possa controllare le attivit� dei propri utenti al di fuori del proprio sito Web, verr� comunque considerata una violazione delle presenti norme l'utilizzo di qualsiasi informazione ottenuta tramite il Servizio per molestare, infastidire o danneggiare altre persone o per contattare un altro utente senza suo previo consenso, adescarlo, inviargli materiale pubblicitario o proporgli la vendita di beni e/o servizi. Allo scopo di proteggere i propri utenti dal ricevere materiale pubblicitario o proposte di vendita di beni e/o servizi, <i>Insaneminds.org</i> si riserva il diritto di limitare le email che l'Utente potr� inviare ad altri utenti in un periodo di 24 ore a un numero che <i>Insaneminds.org</i> riterr� adeguato, a proprio insindacabile giudizio.</li>
				<li><b>Attivit� proibite</b>. <i>Insaneminds.org</i> si riserva il diritto di svolgere indagini e di rescindere il contratto di iscrizione in caso di utilizzo non appropriato del Servizio da parte dell'Utente o di comportamenti tali da essere considerati inappropriati, illeciti o illegali. Di seguito viene riportato un elenco parziale delle azioni che non saranno tollerate nell'utilizzo del Servizio:
				<ol id='listdisc'><li> E vietato impersonare altri soggetti o assumere identit� diverse dalla propria</li>
				<li> E vietato 'perseguitare' o molestare in qualsiasi modo le altre persone.</li>
				<li> E vietato affermare o fare intuire in modo implicito che le proprie dichiarazioni sono approvate da <i>Insaneminds.org</i>, senza previo consenso scritto.</li>
				<li> E vietato l'utilizzo di robot, spider, applicazioni per la ricerca/recupero di siti o di altri dispositivi o applicazioni manuali o automatiche per il recupero, l'indicizzazione e la ricerca di dati. E altres� vietato l'utilizzo di strumenti che possano riprodurre o aggirare la struttura di navigazione e di presentazione del Servizio o dei suoi contenuti.</li>
				<li> E vietato pubblicare, distribuire o riprodurre in qualsiasi modo materiale protetto da copyright, marchi commerciali o altre informazioni proprietarie o protette da propriet� intellettuale senza previo consenso del legittimo proprietario.</li>
				<li> E vietato rimuovere copyright, marchi commerciali o altre indicazioni di diritti proprietari presenti nel Servizio.</li>
				<li> E vietato carpire o produrre siti 'mirror' di parti del Servizio o dell'intero sito Web senza previa autorizzazione scritta da parte di <i>Insaneminds.org</i>. E inoltre vietato l'utilizzo di 'meta tag', codici o altri dispositivi contenenti riferimenti a <i>Insaneminds.org</i> o al Servizio o al sito che conducano a siti Web diversi, per qualsiasi scopo.</li>
				<li> E vietato trasmettere o pubblicare materiale che costituisca o istighi a un comportamento punibile penalmente, che possa dare adito a cause civili o che sia comunque contrario a quanto previsto dalla legge o che violi i diritti di terze parti nel Italia o in qualsiasi altro Paese del mondo.</li>
				<li> Con esclusione, e solo nell'ambito di quanto previsto dalle leggi applicabili, � vietato modificare, adattare, trasferire, tradurre, vendere, decompilare, decifrare o disassemblare in qualsiasi modo qualunque parte del Servizio o del sito Web o di qualsiasi software utilizzato nel sito o per esso. E altres� vietato incaricare altri di compiere tali azioni.</li></ol></li>
				<li><b>Sicurezza dell'account</b>. L'Utente � responsabile di preservare la riservatezza del proprio nome utente e della password indicata al momento della registrazione al Servizio. L'Utente � inoltre responsabile di tutte le azioni compiute con il proprio nome utente e con la password. L'Utente accetta di (a) informare immediatamente <i>Insaneminds.org</i> di eventuali utilizzi non autorizzati del proprio nome utente o della password o di qualsiasi altra violazione della sicurezza e (b) effettuare l'uscita dall'account a conclusione di ciascuna sessione. <i>Insaneminds.org</i> non si assume alcuna responsabilit� per eventuali perdite o danni causati dal mancato adempimento alla presente clausola. In caso di accesso al proprio account da un computer pubblico o condiviso, l'Utente dovr� prestare particolare attenzione per impedire ad altri di vedere o registrare la password o altre informazioni di carattere personale. Se l'Utente condivide il computer con altre persone, potrebbe disattivare la funzione di accesso automatico, nel caso in cui abbia collegato un account Microsoft .Net Passport o AOL ScreenName al proprio account <i>Insaneminds.org</i>.</li>
				<li><b>Assistenza clienti</b>. <i>Insaneminds.org</i> fornisce un servizio di assistenza e guida tramite i propri rappresentanti dell'assistenza clienti. Nel corso delle comunicazioni con i rappresentanti del servizio di assistenza clienti, l'Utente non potr� utilizzare un linguaggio aggressivo, osceno, blasfemo, offensivo, sessista, minaccioso, molesto e razzista. Nel caso in cui il comportamento dell'Utente nei confronti dei rappresentanti del servizio di assistenza clienti o di altri dipendenti risulti minaccioso o offensivo, <i>Insaneminds.org</i> si riserva il diritto di rescindere immediatamente l'iscrizione senza rimborsare in alcun modo l'Utente per l'eventuale mancato utilizzo del Servizio.</li>
				<li><b>Regole sui copyright</b>. E vietato pubblicare, distribuire o riprodurre in qualsiasi modo materiale protetto da copyright, marchi commerciali o altre informazioni proprietarie o protette da propriet� intellettuale senza previo consenso scritto del legittimo proprietario. Senza limitazione a quanto sopra indicato, se l'Utente dovesse ritenere che il proprio lavoro � stato copiato e pubblicato sul Servizio in violazione dei diritti sul copyright, dovr� fornire al nostro addetto ai copyright le seguenti informazioni: una firma fisica o elettronica della persona autorizzata ad agire per conto del proprietario del copyright; una descrizione dell'opera protetta da copyright, oggetto della presunta violazione; una descrizione dell'ubicazione all'interno del sito Web del materiale in possibile violazione; l'indirizzo, il numero telefonico e l'indirizzo email dell'Utente; una dichiarazione scritta in cui l'Utente attesta la propria convinzione che l'oggetto della disputa sia pubblicato senza l'autorizzazione del proprietario del copyright, del suo agente, o di un organo di legge; una lettera firmata dall'Utente che conferma l'accuratezza delle informazioni incluse nella precedente dichiarazione e in cui si dichiara che l'Utente � il proprietario del copyright o che agisce in sua vece. Per qualsiasi reclamo relativo a presunte violazioni del copyright, � possibile contattare l'addetto ai copyright di <i>Insaneminds.org</i> nei modi descritti di seguito: iMatchlegal@<i>Insaneminds.org</i> o all'indirizzo riportato di sopra.</li>
				<li><b>Dispute fra utenti</b>. L'Utente � l'unico responsabile delle proprie interazioni con gli altri utenti di <i>Insaneminds.org</i>. <i>Insaneminds.org</i> si riserva il diritto, ma non si assume alcun obbligo, di monitorare eventuali dispute fra l'Utente e gli altri utenti.</li>
				<li><b>Privacy</b>. L'utilizzo del sito Web e/o del Servizio � governato anche dalla nostra Informativa sulla privacy. Le informazioni personali (incluse quelle sensibili) che l'Utente fornisce a <i>Insaneminds.org</i> verranno conservate nei nostri computer. L'Utente autorizza <i>Insaneminds.org</i> a utilizzare tali informazioni per creare un profilo di interessi, preferenze e schemi di navigazione che consentano la partecipazione ai Servizi. Si informano gli utenti residenti al di fuori degli Stati Uniti che i dati forniti verranno trasferiti negli USA. SI AVVISA L'UTENTE CHE IN GENERE SI CONSIDERA CHE LE NORMATIVE STATUNITENSI NON FORNISCONO UN LIVELLO DI PROTEZIONE DEI DATI 'INFERIORE' RISPETTO A QUANTO PREVISTO DALL'ARTICOLO 25 DELLA DIRETTIVA SULLA SICUREZZA DEI DATI DELL'UNIONE EUROPEA' (95/46/EC). CON L'ISCRIZIONE AL SERVIZIO, L'UTENTE ACCETTA (i) IL TRASFERIMENTO DEI DATI PERSONALI NEGLI STATI UNITI; (ii) IL LORO UTILIZZO IN TALE PAESE E (iii) IL TRASFERIMENTO DEI DATI PERSONALI A TERZE PARTI COINVOLTE NELLA FORNITURA E NELLA GESTIONE DEI SERVIZI, IN CONFORMIT� ALLA NOSTRA INFORMATIVA SULLA PRIVACY.</li>
				<li><b>Accesso al servizio</b>. <i>Insaneminds.org</i> prende tutte le misure necessarie per assicurare l'accessibilit� del proprio sito Web 24 ore al giorno, tuttavia, non garantisce che il servizio sar� ininterrotto o privo di errori.
				L'accesso a questo sito Web potr� essere temporaneamente sospeso senza preavviso in caso di errore del sistema, operazioni di manutenzione o riparazione o per altri motivi al di fuori del nostro controllo.</li>
				<li><b>Negazione di responsabilit�</b>. Il materiale del sito Web viene fornito 'cos� com'�' senza condizioni, garanzie o altri termini di alcun tipo. Pertanto, nei limiti previsti dalla legge, questo sito Web viene fornito all'Utente su queste basi: <i>Insaneminds.org</i> esclude qualsiasi dichiarazione, garanzia, condizione o altro termine (incluse, ma senza limitazione, alle condizioni previste per legge di qualit� soddisfacente, adeguatezza allo scopo e fornitura diligente e competente) che risulterebbe vigente in assenza della presente clausola.
				<i>Insaneminds.org</i> non garantisce n� conferma l'accuratezza o l'affidabilit� di consigli, opinioni, dichiarazioni o altre informazioni visualizzate, caricate o distribuite tramite il Servizio, dai nostri partner, dagli iscritti o da qualsiasi altra persona o entit�. L'Utente accetta di affidarsi a tali opinioni, consigli, dichiarazioni, informazioni o ai profili di altri utenti a proprio esclusivo rischio. <i>Insaneminds.org</i> non si assume alcuna responsabilit� per quanto riguarda il comportamento online o nella vita reale dei propri iscritti. Si prega l'Utente di utilizzare la massima attenzione e il proprio buon senso nell'utilizzo del sito Web e del Servizio. QUESTA INFORMATIVA LEGALE E LA PRECEDENTE NEGAZIONE DI RESPONSABILITA NON PREGIUDICANO IN ALCUN MODO GLI OBBLIGHI DI LEGGE CHE NON POSSONO ESSERE ESCLUSI IN BASE ALLE NORMATIVE APPLICABILI, INCLUSI I DIRITTI COSTITUITI DELL'UTENTE IN QUALIT� DI CONSUMATORE.</li>
				<li><b>Limiti di responsabilit�</b>. Nulla di quanto indicato nel presente avviso legale esclude o limita in alcun modo la responsabilit� di <i>Insaneminds.org</i> in relazione a (i) decessi o lesioni personali causati da negligenza (in virt� di come viene considerato tale termine nell'Unfair Contract Terms Act 1977)]; (ii) frode; (iii) false dichiarazioni su materie rilevanti o (iv) qualsiasi responsabilit� che non possa essere esclusa o limitata in base alle normative di legge applicabili.
				<br />In base a quanto riportato sopra, <i>Insaneminds.org</i> e le societ� del gruppo <i>Insaneminds.org</i> non si assumono alcuna responsabilit� nei confronti dell'Utente o di terzi per eventuali perdite o danni (inclusi, senza limitazione, perdite o danni diretti, indiretti, punitivi o consequenziali, eventuali perdite di profitti, reputazione, dati, contratti, disponibilit� monetaria o perdite o danni dovuti o connessi a qualsiasi genere di interruzione dell'attivit� lavorativa, sia in caso di torto (inclusa, senza limitazione, la negligenza), contratto o altro in relazione al Servizio o al Sito Web oppure all'utilizzo, all'impossibilit� di utilizzare o ai risultati dell'utilizzo del Sito Web.
				<br />In base a quanto riportato sopra, la responsabilit� di <i>Insaneminds.org</i> nei confronti dell'Utente, per qualsiasi causa e a prescindere dalla forma dell'azione, sar� sempre limitata all'eventuale importo versato dall'Utente a <i>Insaneminds.org</i> per l'utilizzo del Servizio nel periodo di durata dell'iscrizione.</li>
				<li><b>Emendamenti al contratto</b>. <i>Insaneminds.org</i> si riserva il diritto di modificare o emendare questi termini e condizioni, fornendo all'Utente un ragionevole preavviso prima che le modifiche o gli emendamenti vengano applicati. Se, a seguito della ricezione di un preavviso, l'utente dovesse decidere di non continuare a utilizzare il Servizio, potr� rescindere il contratto informando <i>Insaneminds.org</i> con un avviso che risulter� applicabile dal momento in cui <i>Insaneminds.org</i> lo ricever� oppure dalla data in cui sarebbero entrati in vigore gli emendamenti ai termini e alle condizioni, in base a quale dei due eventi si verifichi per primo. Se l'Utente dovesse continuare a utilizzare il Servizio dopo la scadenza del suddetto periodo, tutte le modifiche e/o gli emendamenti si considereranno accettati.</li>
				<li><b>Propriet� dei diritti</b>. Tutti i materiali inclusi nel sito Web sono protetti da copyright, da trattati e dalle leggi internazionali sul copyright. Tutti i diritti riservati. I materiali di <i>Insaneminds.org</i> non potranno essere in alcun modo riprodotti, copiati, modificati, pubblicati, trasmessi o caricati senza previo consenso scritto da parte di <i>Insaneminds.org</i>. Con esclusione di quanto espressamente previsto nelle clausole della licenza limitata del presente Contratto, <i>Insaneminds.org</i> non fornisce all'Utente alcun diritto, esplicito o implicito, in relazione ai propri marchi commerciali, ai copyright o ad altre propriet� intellettuali o informazioni proprietarie.</li>
				<li><b>Esonero</b>. Nel caso in cui l'Utente avvii un reclamo o un'azione legale nei confronti di un altro utente in relazione all'utilizzo da parte di quest'ultimo del sito Web o del Servizio, l'Utente dichiara di intraprendere tali azioni a titolo personale e senza alcuna richiesta da parte nostra ed esonera <i>Insaneminds.org</i> da eventuali reclami, responsabilit� e danni che potrebbero risultare o essere in qualsiasi modo correlati a suddetto reclamo o azione legale.
				<br />Nel caso in cui sia presentato un reclamo o un'azione legale venga avviata nei nostri confronti a seguito dell'utilizzo del sito Web o del Servizio da parte dell'Utente, incluse eventuali violazioni del presente contratto o accuse e reclami presentati da terze parti nei confronti dell'Utente, l'Utente accetta di rifondere, tenere indenne e difendere <i>Insaneminds.org</i> da tali reclami o azioni legali. L'Utente accetta inoltre di fornire completa collaborazione per quanto ragionevolmente richiesto nella difesa di eventuali reclami e di consentirci di assumere difesa e controllo esclusivi della questione.</li>
				<li><b>Separazione</b>. Se una qualsiasi parte del presente Contratto viene ritenuta non valida o non applicabile in base alle leggi vigenti incluse, senza limitazione, le negazioni di responsabilit� sulla garanzia e limitazioni di responsabilit� sopra specificate, la clausola non valida o non applicabile verr� sostituita da una clausola valida e applicabile che si avvicini quanto pi� possibile allo spirito della clausola originale. La parte restante del Contratto rimarr� valida.</li>
				<li><b>Assegnazione</b>. A differenza dell'Utente, <i>Insaneminds.org</i>. pu� assegnare, trasferire, cedere in subappalto o delegare i diritti, i doveri o gli obblighi oggetto del presente Contratto.</li>
				<li><b>Relazioni</b>. L'Utente dichiara che fra se stesso e <i>Insaneminds.org</i> non sussistano rapporti di joint venture, partnership, dipendenza o di collaborazione come conseguenza del presente Contratto o dell'utilizzo del sito Web.</li>
				<li><b>Controversie</b>. In caso di controversia in relazione al sito Web e/o al Servizio derivante dall'utilizzo del sito Web, l'Utente accetta che tale controversia verr� giudicata dalle leggi dell'Italia, a prescindere dagli eventuali conflitti con le disposizioni di legge. L'Utente accetta che la giurisdizione personale sia attribuita ai tribunali di Genova, Italia.</li>
				<li><b>Altro</b>. Il presente Contratto, accettato tramite l'utilizzo del sito Web e confermato con l'iscrizione al Servizio, costituisce l'accordo completo fra l'Utente e <i>Insaneminds.org</i> in relazione all'utilizzo del sito Web e/o del Servizio. Se una qualsiasi clausola del presente Contratto dovesse risultare non valida, la parte restante del Contratto risulter� in vigore ed applicabile.</li>
				<li><b>Informazioni su come contattarci</b>. Per qualsiasi domanda sui diritti e le limitazioni riportati sopra, puoi contattarci all'indirizzo imatchlegal@<i>Insaneminds.org</i> oppure cliccare qui per ottenere aiuto.</li></ol>
				<br />
				Per qualsiasi domanda inerente al contratto, puoi contattarci. <i>Insaneminds.org</i> � un marchio commerciale di <i>Insaneminds.org</i><br />
				<br />
				<b>DICHIARO DI AVERE LETTO IL CONTRATTO E DI ACCETTARE TUTTE LE CLAUSOLE IN ESSO CONTENUTE.</b>
				<br /><br /><h3>Informazioni sul trattamento dei dati personali e sulla privacy</h3><br /><br />
				Ai sensi dell'art. 10 della legge n. 675 del 31 dicembre 1996 ed in relazione ai dati personali che si intendono trattare, <i>Insaneminds.org</i> la informa di quanto segue:<br />
				<br />
				I dati personali liberamente comunicati saranno registrati su database elettronici, protetti e non accessibili al pubblico.<br />
				<br />
				Proprietario del database e responsabile per il trattamento dei dati � :<br />
				<br /><p id='center'>
				<i>Insaneminds.org<br />
				Via A. Liri 31<br />
				16145 Genova</i></p>
				<br />
				Il trattamento dei dati forniti avviene per le seguenti finalit� :<br />
				<br /><ol id='listalpha'>
				<li> fornire i servizi previsti e rilevarne il grado di soddisfazione;</li>
				<li> informarla sui nuovi prodotti e servizi;</li>
				<li> gestire le attivit� di marketing, promozionali e pubblicitarie relative a prodotti e servizi del festival Be-You;</li>
				<li> ottemperare agli obblighi di legge;</li>
				<li> collaborazione attiva al festival.</li>
				</ol><br />
				I suoi dati personali potranno essere comunicati da <i>Insaneminds.org</i>, ai sensi dell'art. 20 della Legge:<br />
				<br /><ol id='listdisc'>
				<li> a societ� controllate o collegate a <i>Insaneminds.org</i> ai sensi dell'art. 2359 codice civile</li>
				<li> persone fisiche o giuridiche che per conto e/o nell'interesse di <i>Insaneminds.org</i> effettuino specifici servizi elaborativi o svolgano attivit� connesse o di supporto a quelle di <i>Insaneminds.org</i></li>
				</ol><br />
				In nessun altro caso i suoi dati personali saranno da noi trasmessi a terze parti, eccezion fatta nei casi in cui <i>Insaneminds.org</i> :<br />
				<br /><ol id='listdisc'>
				<li> ha avuto il suo consenso esplicito a condividere i suoi dati con terze parti;</li>
				<li> deve ottemperare ad ordini scritti dell'Autorit� Giudiziaria;</li>
				</ol><br />
				In conformit� con l'art. 13 della Legge 675 del 31/12/1996, l<i>Insaneminds.org</i> le riconosce i seguenti diritti :<br />
				<br /><ol id='listalpha'>
				<li> di accedere in qualsiasi momento ai suoi dati;</li>
				<li> di ottenere in qualsiasi momento: <ol id='listnum'>
				<li> la conferma dell'esistenza o meno di dati personali che la riguardano, anche se non ancora registrati, e la comunicazione in forma intellegibile dei medesimi dati e della loro origine;</li>
				<li> la cancellazione, la trasformazione in forma anonima o il blocco dei dati trattati in violazione di legge, nonch� l'aggiornamento, la rettificazione ovvero, qualora vi sia l'interesse, l'integrazione dei dati;</li>
				<li> l'attestazione che le operazioni di cui al punto 2b sono state portate a conoscenza, anche per quanto riguarda il loro contenuto, di coloro ai quali i dati sono stati comunicati o diffusi;</li>
				<li> di opporsi per motivi legittimi, al trattamento dei dati;</li>
				<li> di opporsi ai trattamenti per finalit� commerciali, pubblicitarie o di ricerche di mercato;</li>
				</ol></li></ol><br />
				Per esercitare i suddetti diritti pu� scrivere a :<br />
				<br /><p id='center'>
				<i>Insaneminds.org<br />
				Via A. Liri 31<br />
				16145 Genova</i></p>";
		}
}
?>