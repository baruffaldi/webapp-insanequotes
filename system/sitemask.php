<?php
/* $Id: sitemask.php v. 2.1.7 01:42 15/04/2007 mdb Exp $
 * $Author: mdb $
 *
 * www.Baruffaldi.Info XHTML Structure Functions
 *
 * Written by Filippo Baruffaldi (filippo@baruffaldi.info)
 *
 * PHP released under Creative Commons Attribution-Noncommercial-Share
 * Alike 2.5 Italy License :: http://creativecommons.org/licenses/by-nc-sa/2.5/it/
*/

class IQ_SITEMASK
{
		function htmlinit($title_pag)
		{
				print IQ_SITEMASK::htmlheaders();
				print IQ_SITEMASK::htmlhead();
				print IQ_SITEMASK::htmlbody($title_pag);
		}



		function htmlheaders()
		{
				// HTTP/1.1 headers (no-cache)
				header("Cache-Control: no-store, no-cache, must-revalidate");
				header("Cache-Control: post-check=0, pre-check=0", false);

				// HTTP/1.0 headers (no-cache)
				header("Pragma: no-cache");

				// Declare the Content-Type and the charset
				header('Content-type: text/html; charset=iso-8859-1');

				return "
<!--
/* \$Id: index.php,v ".TXT_SITE_VERSION." ".TXT_SITE_LASTUPDATE." 07:27:03 mdb Exp $
 * \$Author: mdb $
 *
 * ".SITE_HOSTNAME." XHTML Template
 *
 * Written by Filippo Baruffaldi (filippo@baruffaldi.info)
 *
 * XHTML released under Creative Commons Attribution-Noncommercial-Share Alike 2.5 Italy License
 * http://creativecommons.org/licenses/by-nc-sa/2.5/it/
*/
-->

<?xml version='1.0' encoding='ISO-8859-1' ?>
<!DOCTYPE html
		 PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN'
		 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>";
		}



		function htmlhead()
		{
				if (TXT_SITE_PAGENAME != "") define('TXT_SITE_PAGENAME', " :: ".TXT_SITE_PAGENAME);
				
				return  "
<html xmlns='http://www.w3.org/1999/xhtml' xml:lang='".SITE_LANG."' lang='".SITE_LANG."'>
		<head>
				<meta http-equiv='Content-Type' content='text/html;charset=iso-8859-1' />

				<title>".SITE_HOSTNAME." :: ".TXT_SITE_NAME.TXT_SITE_PAGENAME."</title>

				<meta name='title'		content='".TXT_SITE_NAME."' />
				<meta name='keywords'		content='".TXT_SITE_KEYWORDS."' />
				<meta name='description'	content='".TXT_META_DESC1."
									 ".TXT_META_DESC2.".' />
									 
				<meta name="verify-v1" content="8quRaW+4Um7aflPljOZWHlsrtTQH8iP2WJN4a/iEkgs=" />
				<meta name='robots'		content='index' />
				<meta name='date'		content='10Apr2007' />
				<meta name='revisit-after'	content='15 Days' />

				<meta name='subject'		content='".TXT_SITE_NAME."' />
				<meta name='author'		content='Filippo Baruffaldi' />
				<meta name='publisher'		content='Filippo Baruffaldi' />
				<meta name='reply-to'		content='filippo@baruffaldi.info' />

				<link rev='made'		href='mailto:filippo@baruffaldi.info' />
				<link rel='shortcut icon'	href='images/site_icon.ico' type='image/icon' />
				
				<style type='text/css'>
					/* Import the layouts stylesheet */
					@import 'css/layouts.css';

					/* Import the graphics stylesheet */
					@import 'css/graphics.css';
				</style>

				
				
				<!--[if lt IE 7]>
				<style type='text/css'>
				#content_search
				{
					background:none;
					filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/site_con_search.png' ,sizingMethod='crop');
				}
				#content_search_registered
				{
					background:none;
					filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/site_con_search_reg.png' ,sizingMethod='crop');
				}
				#content_mainWindow_head
				{
					background:none;
					filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/site_con_mainWindow_head.png' ,sizingMethod='crop');
				}
				#content_mainWindow_body
				{
					background:none;
					filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/site_con_mainWindow_body.png' ,sizingMethod='scale');
				}
				#content_mainWindow_foot
				{
					background:none;
					filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/site_con_mainWindow_foot.png' ,sizingMethod='scale');
				}
				#navigation_logo
				{
					background:none;
					filter:progid:DXImageTransform.Microsoft.AlphaImageLoader(src='images/site_logo.png' ,sizingMethod='crop');
					width: 410px;
					height: 105px;
				}
				</style>
				<![endif]-->
				
				
		</head>";
		}



		function htmlbody()
		{
				$ret = "
		<body>
				<div id='container'>

						<div id='navigation'>
								<div id='navigation_logo'><!--[if !lt IE 7]>
								<a href='/' title='InsaneQuotes :: Le migliori citazioni della rete!'><img id='site_logo' src='images/site_logo.png' alt='InsaneQuotes :: Le migliori citazioni della rete!' /></a>
								<![endif]-->
								<!--[if IE]><![if !IE]><![endif]-->
								<a href='.' title='InsaneQuotes :: Le migliori citazioni della rete!'><img id='site_logo' src='images/site_logo.png' alt='InsaneQuotes :: Le migliori citazioni della rete!' /></a>
								<!--[if IE]><![endif]><![endif]-->
								</div>
								<div id='navigation_menu'>
										<dl id='navigation_menu_list'>";
				if (ID == 0) $ret .= "
											<dd id='navigation_menu_item'><a href='./?register' title='InsaneQuotes :: Registrata un account InsaneMinds'><img class='site_nav_button' src='images/site_nav_bot_register.png' alt='InsaneQuotes :: Registrata un account InsaneMinds' /></a></dd>";
				$ret .= "
											<dd id='navigation_menu_item'><a href='./?lastquotes' title='InsaneQuotes :: Ultime citazioni inserite'><img class='site_nav_button' src='images/site_nav_bot_last.png' alt='InsaneQuotes :: Ultime citazioni inserite' /></a></dd>
											<dd id='navigation_menu_item'><a href='./?randquotes' title='InsaneQuotes :: Citazioni casuali'><img class='site_nav_button' src='images/site_nav_bot_rand.png' alt='InsaneQuotes :: Citazioni casuali' /></a></dd>
											<dd id='navigation_menu_item'><a href='./?rankquotes' title='InsaneQuotes :: Classifica'><img class='site_nav_button' src='images/site_nav_bot_rank.png' alt='InsaneQuotes :: Classifica' /></a></dd>
											<dd id='navigation_menu_item'><a href='./?newquote' title='InsaneQuotes :: Inserisci una nuova citazione'><img class='site_nav_button' src='images/site_nav_bot_new.png' alt='InsaneQuotes :: Inserisci una nuova citazione' /></a></dd>
											<dd id='navigation_menu_item'><a href='./?feedback' title='InsaneQuotes :: Contatti'><img class='site_nav_button' src='images/site_nav_bot_us.png' alt='InsaneQuotes :: Contatti' /></a></dd>
										</dl>
								</div>
						</div>

						<div id='content'>";
						if (ID != 0) {
						$ret .= "
								<div id='content_search_registered'>
										<form method='post' action='?search'>
										<input type='text'   name='search' size='12' maxlength='37' id='input_search' />
										<input id='input_submit_search' value='' type='submit' />
										</form><br />
										<p><img src='http://www.insaneminds.org/getimage.php?type=1&size=20&color=82b0f9&txt=".NICKNAME."' alt='Profilo: ".NICKNAME."' /></p><a href='?logout' title='InsaneQuotes :: Esci'><img src='images/site_con_logout_icon.gif' alt='InsaneQuotes :: Esci' /></a>
								</div>";
						} else {
						$ret .= "
								<div id='content_search'>
										<form method='post' action='?search'>
										<input type='text'   name='search' size='12' maxlength='37' id='input_search' />
										<input id='input_submit_search' value='' type='submit' />
										</form><br />
										<form method='post' action='{$_SERVER[REQUEST_URI]}'>
										<input type='text'   name='username' size='10' maxlength='37' id='input_username' />
										<input type='password'   name='user_password' size='10' maxlength='37' id='input_password' />
										<input id='input_submit_login' value='' type='submit' />
										</form>
								</div>";
						}

				return $ret;
		}



		function htmlopenwindow($title_page)
		{
				return "
								<div id='content_mainWindow'>
									<div id='content_mainWindow_head'>
										<p><b>$title_page</b></p>
									</div>
									<div id='content_mainWindow_body'>
										<div id='content_mainWindow_body_content'>";
		}


		function htmlclosewindow()
		{
				return "
										</div>
									</div>
									<div id='content_mainWindow_foot'></div>
								</div>";
		}



		function htmlfooter($time_end)
		{
				global $post;

				return "
								<div id='footer'>
										<p>".TXT_FOOTER_COPY."</p>
								</div>
						</div>


				</div>

		</body>

</html>";
		}
}
?>