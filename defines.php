<?php
/* $Id: defines.php v. 0.0.1 01:39 15/04/2007 mdb Exp $
 * $Author: mdb $
 *
 * www.insaneQuotes.co.nr Defines File
 *
 * Written by Filippo Baruffaldi (filippo@baruffaldi.info)
 *
 * PHP released under Creative Commons Attribution-Noncommercial-Share
 * Alike 2.5 Italy License :: http://creativecommons.org/licenses/by-nc-sa/2.5/it/
*/

if (!isset($_SESSION))
{
		session_start();
}

/*
  * Creating Global Variables
*/
$time = explode(':', gmstrftime("%T", time()));
$chiavipost = array_keys($_POST);
$chiaviget = array_keys($_GET);
foreach ($_POST as $key => $value) {
		if ($key != "file") $post .= "$key=$value&";
}

/*
  * Creating Global Costants
*/
define('PATH', str_replace(basename($_SERVER[SCRIPT_NAME]), "", $_SERVER[SCRIPT_NAME]));
define('BASELINK', "http://{$_SERVER['HTTP_HOST']}" . str_replace("index.php", "", str_replace("actions/", "", $_SERVER['PHP_SELF'])));
define('TIME', $time[0].':'.$time[1].':'.$time[2]);
define('DATE', date("y-m-d"));
define('BUFF', "1024");
define('IP', $_SERVER[REMOTE_ADDR]);
define('SITE_HOSTNAME', $_SERVER['HTTP_HOST']);
define('SITE_LANG', "it");

/* APACHE INI MODIFICATIONS */
ini_set('arg_separator.output','&amp;');


/*
  * Lists sorting method
  *
*/
if (empty($_GET[sort])) {$sort = "id";} else {$sort = $_GET[sort];}

?>